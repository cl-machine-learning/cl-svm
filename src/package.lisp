(defpackage :cl-svm
  (:nicknames :svm)
  (:use :cl)
  (:export #:simple-train-svm
	   #:training-pair-bind
	   #:dot-product))
